FROM registry.gitlab.com/dan_miles_pwc/grpc-python/master:latest

LABEL maintainer="daniel.miles@pwc.com"

WORKDIR /api

COPY requirements.txt requirements.txt

RUN  pip install -r requirements.txt

COPY . .

ENTRYPOINT ["uvicorn", "main:app", "--host", "0.0.0.0", "--port", "8000"]