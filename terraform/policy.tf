resource "google_compute_security_policy" "default-drop" {
  name = "default-drop"
  description = "Default drops all traffic"

  # Reject all traffic that hasn't been whitelisted.
  rule {
    action = "deny(403)"
    priority = "2147483647"

    match {
      versioned_expr = "SRC_IPS_V1"

      config {
        src_ip_ranges = [
          "*"]
      }
    }
    description = "Default rule, higher priority overrides it"
  }

  rule {
    action = "allow"
    priority = "100"
    description = "WAF Enforcement"
    match {
      expr {
        expression = "evaluatePreconfiguredExpr('xss-stable')"
      }
    }
  }

  rule {
    action = "allow"
    priority = "1000"

    match {
      versioned_expr = "SRC_IPS_V1"

      config {
        src_ip_ranges = ["8.8.8.8"]
      }
    }
    description = "Whitelist known good traffic"
  }
}

resource "google_compute_ssl_policy" "default-ssl-policy" {
  name            = "default-ssl-policy"
  profile         = "MODERN"
  min_tls_version = "TLS_1_2"
}