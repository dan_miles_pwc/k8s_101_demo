resource "google_dns_managed_zone" "cyberthreatops" {
  dns_name = "cyberthreatops.com."
  name = "cyberthreatops"
  visibility = "public"
}

resource "google_dns_record_set" "macchiato" {
  managed_zone = google_dns_managed_zone.cyberthreatops.name
  name = "macchiato.${google_dns_managed_zone.cyberthreatops.dns_name}"
  rrdatas = [
    ""]
  ttl = 300
  type = "A"
}