from fastapi import FastAPI
import os
import logging
import elasticapm
from elasticapm.contrib.starlette import make_apm_client, ElasticAPM

logging_conf_path = os.path.normpath(os.path.join(os.path.dirname(__file__), "logging.conf"))
logging.config.fileConfig(logging_conf_path)
logger = logging.getLogger(__name__)

apm = make_apm_client({
    'SERVICE_NAME': 'Demo API',
    'SECRET_TOKEN': os.environ.get('APM_SECRET_KEY', '')
})

app = FastAPI()
app.add_middleware(ElasticAPM, client=apm)
elasticapm.instrument()


@app.get("/greeting")
def read_main():
    return {"message": "Hello World!"}


@app.get("/healthz")
def read_main():
    return {"message": "OK"}
